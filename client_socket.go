package akaseea

import (
	"bufio"
	"fmt"
	"net"
	"time"

	"github.com/gorilla/websocket"
)

// TODO 還要檢查UDP
func (hub *Hub[META, HEADER]) SocketHandles(addr, port string, proto ConnectionProtocal) {

	switch proto {
	case TCP:
		listen, err := net.Listen(string(proto), addr+":"+port)
		if err != nil {
			hub.setting.Logger.Fatal("listen failed, err:%v\n", err)
		}
		hub.TCPConn(listen)
	case UDP:
		// TODO
		panic("UDP is not ready")

		listen, err := net.ListenUDP("udp", &net.UDPAddr{
			IP:   net.IPv4(0, 0, 0, 0),
			Port: 9090,
		})

		if err != nil {
			hub.setting.Logger.Fatal("listen failed, err:%v\n", err)
		}

		client := &Client[META, HEADER]{
			ID:            hub.idAllocator.allotmentConnectionID(),
			hub:           hub,
			websocketConn: nil,
			write:         make(chan messagePack, hub.setting.ChanQueueSize),
			meta:          hub.setting.MetaGen(),
			ReceiveBuffer: make([]byte, 0),
		}

		// TODO read
		n, addr, err := listen.ReadFromUDP(client.ReceiveBuffer)

		// TODO write
		_, err = listen.WriteToUDP(client.ReceiveBuffer[:n], addr)
		if err != nil {
			fmt.Printf("send data error:%v\n", err)
			return
		}
	}

	hub.setting.Logger.Fatal("unsupper protocal %s", proto)
}

func (hub *Hub[META, HEADER]) TCPConn(listen net.Listener) {
	for {
		conn, err := listen.Accept()
		if err != nil {
			fmt.Printf("accept failed, err:%v\n", err)
			continue
		}
		client := &Client[META, HEADER]{
			ID:            hub.idAllocator.allotmentConnectionID(),
			hub:           hub,
			websocketConn: nil,
			socketConn:    conn,
			write:         make(chan messagePack, hub.setting.ChanQueueSize),
			meta:          hub.setting.MetaGen(),
			ReceiveBuffer: make([]byte, hub.setting.SocketPeriod.MessageBufferSize),
		}

		go client.socketWritePump()
		go client.socketReadPump()
		client.hub.clientRegister(client)
	}
}

func (c *Client[META, HEADER]) socketReadPump() {
	defer func() {
		c.hub.clientUnregister(c)
	}()

	for {

		reader := bufio.NewReader(c.socketConn)
		_, err := reader.Read(c.ReceiveBuffer)
		if err != nil {
			c.Logger().Warn(err.Error())
			return
		}
		err = c.subMatcher.Receive(c, c.ReceiveBuffer)
		if err != nil {
			c.Logger().Warn(err.Error())
		}

	}
}

func (c *Client[META, HEADER]) socketWritePump() {
	ticker := time.NewTicker(c.hub.setting.SocketPeriod.PingPeriod)
	defer func() {
		ticker.Stop()
		c.websocketConn.Close()
	}()
	for {
		select {
		case message, ok := <-c.write:
			// TODO
			// c.websocketConn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.websocketConn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.websocketConn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message.message)

			// Add queued chat messages to the current websocket message.
			n := len(c.write)
			for i := 0; i < n; i++ {
				w.Write(c.hub.setting.WebsocketPeriod.WriteNewline)
				m := <-c.write
				w.Write(m.message)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			// c.websocketConn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.websocketConn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}
