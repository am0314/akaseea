package akaseea

import (
	"time"

	"github.com/gorilla/websocket"
)

type SocketPeriod struct {
	// Send pings to peer with this period. Must be less than pongWait.
	PingPeriod time.Duration

	// PongWait sets the read deadline on the underlying network connection.
	// After a read has timed out, the websocket connection state is corrupt and
	// all future reads will return an error. A zero value for t means reads will
	// not time out.
	PongWait time.Duration

	// Time allowed to write a message to the peer.
	// sets the write deadline on the underlying network.
	// connection. After a write has timed out, the websocket state is corrupt and
	// all future writes will return an error. A zero value for t means writes will
	// not time out.
	WriteWait time.Duration

	// WsPongHandler sets the handler for pong messages received from the peer.
	// The appData argument to h is the PONG message application data. The default
	// pong handler does nothing.
	//
	// The handler function is called from the NextReader, ReadMessage and message
	// reader Read methods. The application must read the connection to process
	// pong messages as described in the section on Control Messages above.
	WsPongHandler func(c *websocket.Conn) func(string) error

	// setting when write time out handle
	WsWriteTimeOutHandle func(c *websocket.Conn)

	// WriteMessageType
	//  TextMessage   messageType = websocket.TextMessage
	//  BinaryMessage messageType = websocket.BinaryMessage
	//  CloseMessage  messageType = websocket.CloseMessage
	//  PingMessage   messageType = websocket.PingMessage
	//  PongMessage   messageType = websocket.PongMessage
	//  default TextMessage = 1
	WriteMessageType messageType

	// MessageBufferSize sets the maximum size in bytes for a message read from the peer. If a
	// message exceeds the limit, the connection sends a close message to the peer
	// and returns ErrReadLimit to the application.
	//  default 1KB
	MessageBufferSize DataSize
}
