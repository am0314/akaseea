package akaseea

import "sync/atomic"

type ClientID uint64
type HotlineID uint64

type idAllotment struct {
	connIDAllotment    ClientID
	hotlineIDAllotment HotlineID
}

func (cid *idAllotment) allotmentConnectionID() ClientID {
	u := uint64(cid.connIDAllotment)
	cid.connIDAllotment = ClientID(atomic.AddUint64(&u, 1))
	return cid.connIDAllotment
}

func (cid *idAllotment) allotmentHotlineID() HotlineID {
	u := uint64(cid.hotlineIDAllotment)
	cid.hotlineIDAllotment = HotlineID(atomic.AddUint64(&u, 1))
	return cid.hotlineIDAllotment
}
