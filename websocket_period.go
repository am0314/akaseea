package akaseea

import (
	"bytes"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

type messageType int

const (
	TextMessage   messageType = websocket.TextMessage
	BinaryMessage messageType = websocket.BinaryMessage
	CloseMessage  messageType = websocket.CloseMessage
	PingMessage   messageType = websocket.PingMessage
	PongMessage   messageType = websocket.PongMessage
)

type WebsocketPeriod[META any] struct {
	// Upgrader
	//  default &websocket.Upgrader{
	// 	HandshakeTimeout: 1,
	// 	ReadBufferSize:   int(KB * 1),
	// 	WriteBufferSize:  int(KB * 1),
	// 	WriteBufferPool: &sync.Pool{
	// 		New: func() any {
	// 			return make([]byte, KB)
	// 		},
	// 	},
	// 	Subprotocols: []string{},
	// 	Error: func(w http.ResponseWriter, r *http.Request, status int, reason error) {
	// 	},
	// 	CheckOrigin: func(r *http.Request) bool {
	// 		return true
	// 	},
	// 	EnableCompression: false,
	// }
	Upgrader *websocket.Upgrader

	PingPongPlan PingPongPlan[META]

	// WriteMessageType
	//  TextMessage   messageType = websocket.TextMessage
	//  BinaryMessage messageType = websocket.BinaryMessage
	//  CloseMessage  messageType = websocket.CloseMessage
	//  PingMessage   messageType = websocket.PingMessage
	//  PongMessage   messageType = websocket.PongMessage
	//  default TextMessage = 1
	WriteMessageType messageType
	ExpectedWsError  []int
	// WriteNewline 寫入時的換行符號
	WriteNewline []byte
	// ReadBufferPreprocessing 必須 可用 ReadBufferPreprocessingText ReadBufferPreprocessingBinary 或自定義
	ReadBufferPreprocessing ReadBufferPreprocessing
}
type ReadBufferPreprocessing func([]byte) []byte

// TextBufferPreprocessing 處理文本時的預處理模式 例如json 會有換行符號
func ReadBufferPreprocessingText(b []byte) []byte {
	return bytes.TrimSpace(bytes.ReplaceAll(b, []byte("\n"), []byte(" ")))
}

// BinaryBufferPreprocessing 處理二進制時的預處理模式 例如pb 就是不處理
func ReadBufferPreprocessingBinary(b []byte) []byte {
	return b
}

type PingPongPlan[META any] struct {

	//PingHandler The default situation is that the client initiates a ping to maintain the connection.
	PingHandler func(ictx IOContext, m META, pingMsg string) error

	// After how long after pinging, if no ping is received, it will be timed out.
	PingTimeout time.Duration

	// PongHandler What to do when you receive a pong
	PongHandler func(ictx IOContext, m META, pongMsg string) error
}

func DefaultPingHandler[MEAT any](i IOContext, m MEAT, pingMsg string) error {
	i.WriteMessageType(int(PongMessage), []byte(pingMsg))
	if err := i.WsConn().SetReadDeadline(time.Now().Add(60 * time.Second)); err != nil {
		return err
	}
	return nil
}

// DefaultWebsocketPeriod 注意ReadBufferPreprocessing 只適用於json等文本類型
func DefaultWebsocketPeriod[MEAT any]() *WebsocketPeriod[MEAT] {
	const (
		defaultPingPeriod  = 60 * time.Second
		defaultPingTimeout = 10 * time.Second
	)
	return &WebsocketPeriod[MEAT]{
		Upgrader: DefaultUpgrader(),
		PingPongPlan: PingPongPlan[MEAT]{
			PingTimeout: defaultPingTimeout,
			PongHandler: func(i IOContext, m MEAT, pongMsg string) error {
				return nil
			},
			PingHandler: DefaultPingHandler[MEAT],
		},
		WriteMessageType: websocket.TextMessage,
		ExpectedWsError: []int{
			websocket.CloseNormalClosure,
			websocket.CloseGoingAway,
			websocket.CloseAbnormalClosure},
		ReadBufferPreprocessing: ReadBufferPreprocessingText,
		WriteNewline:            []byte{'\n'},
	}
}

func DefaultUpgrader() *websocket.Upgrader {
	return &websocket.Upgrader{
		HandshakeTimeout: 1,
		ReadBufferSize:   int(KB * 1),
		WriteBufferSize:  int(KB * 1),
		WriteBufferPool: &sync.Pool{
			New: func() any {
				return make([]byte, KB)
			},
		},
		Subprotocols: []string{},
		Error: func(w http.ResponseWriter, r *http.Request, status int, reason error) {
		},
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
		EnableCompression: false,
	}
}
