package akaseea

import (
	"reflect"
)

func (matcher *MatcherMap[META, HEADER]) Init(logger ILogger) {
	matcher.handleMap = make(map[HEADER]HandlePack)
	matcher.routeMap = make(map[HEADER]string)

	for header, handle := range matcher.HandleSetting {
		if handle == nil {
			logger.Warn("Handle is nil, header: ", header)
			continue
		}

		handleFuncName, handleFunc, handleRequest, err := handleCheck(handle, header)
		if err != nil {
			logger.Error(err.Error())
			continue
		}

		matcher.routeMap[header] = handleFuncName

		nextHandlePack := HandlePack{
			Handle:            handleFunc,
			ParsingBox:        handleRequest,
			SkipSerialization: reflect.TypeOf(handleRequest) == reflect.TypeOf(empty),
		}

		matcher.handleMap[header] = nextHandlePack

	}
}

func (matcher *MatcherMap[META, HEADER]) GenSub() SubMatcher[META, HEADER] {
	handleCopy := make(map[HEADER]HandlePack)
	for k, v := range matcher.handleMap {

		tof := reflect.ValueOf(v.ParsingBox).Elem().Type()

		handleCopy[k] = HandlePack{
			Handle:            v.Handle,
			ParsingBox:        reflect.New(tof).Interface(),
			SkipSerialization: v.SkipSerialization,
		}

	}

	return SubMatcher[META, HEADER]{
		master:    matcher,
		handleMap: handleCopy,
	}
}

func (matcher *MatcherMap[META, HEADER]) HeaderParsing(ReceiveData []byte) (HEADER, []byte) {
	return matcher.HeaderParsingFunc(ReceiveData)
}

func (matcher *MatcherMap[META, HEADER]) Unmarshal(body []byte, handleRequest any) error {
	return matcher.UnmarshalFunc(body, handleRequest)
}

func (matcher *MatcherMap[META, HEADER]) Marshal(handleRequest any) ([]byte, error) {
	return matcher.MarshalFunc(handleRequest)
}
