package akaseea

import (
	"context"
	"errors"
)

// ExecSync sends work to the main thread of the Hub to ensure that there are no data race conflicts.
//
// and return error
func (hub *Hub[META, HEADER]) ExecSync(work func(DHub) error) error {
	ack := make(chan error, 1)
	select {
	case <-hub.Context().Done():
		return errors.New("hub is close")
	default:
		defer close(ack)
		hub.internal <- func(h *Hub[META, HEADER]) {
			ack <- work(hub)
		}
	}
	return <-ack
}

// ExecAsync sends work to the main thread of the Hub to ensure that there are no data race conflicts.
func (hub *Hub[META, HEADER]) ExecAsync(work func(DHub)) {
	select {
	case <-hub.Context().Done():
	default:
		hub.internal <- func(h *Hub[META, HEADER]) {
			work(h)
		}
	}
}

func (hub *Hub[META, HEADER]) BroadcastWithFilter(data []byte, filter func(IHub, IOContext, META) bool) {
	select {
	case <-hub.Context().Done():
	default:
		hub.internal <- func(h *Hub[META, HEADER]) {
			for _, client := range hub.clientBus {
				if filter(hub, client, client.(*Client[META, HEADER]).meta) {
					client.Write(data)
				}
			}
		}
	}
}
func (hub *Hub[META, HEADER]) allotmentHotlineID() HotlineID {
	return hub.idAllocator.allotmentHotlineID()
}

func (hub *Hub[META, HEADER]) WaitAdd(num int) {
	hub.terminalStation.Add(num)
}
func (hub *Hub[META, HEADER]) WaitDone() {
	hub.terminalStation.Done()
}
func (hub *Hub[META, HEADER]) Context() context.Context {
	return hub.setting.ContextOption
}
func (hub *Hub[META, HEADER]) Marshal(any any) ([]byte, error) {
	return hub.setting.ReceiveMatcher.Marshal(any)
}

func (hub *Hub[META, HEADER]) Unmarshal(data []byte, any any) error {
	return hub.setting.ReceiveMatcher.Unmarshal(data, any)
}
func (hub *Hub[META, HEADER]) HubBroadcast(data []byte) {
	select {
	case <-hub.Context().Done():
	default:
		hub.internal <- func(h *Hub[META, HEADER]) {
			for _, client := range hub.clientBus {
				client.Write(data)
			}
		}
	}
}

func (hub *Hub[META, HEADER]) clientAnswer(pack clientHotlineActionPack) {
	select {
	case <-hub.Context().Done():
	default:
		hub.internal <- func(h *Hub[META, HEADER]) {
			pack.hotlineExp()[pack.hotline.ID] = pack.hotline
		}
	}
}

func (hub *Hub[META, HEADER]) clientHangup(pack clientHotlineActionPack) {
	select {
	case <-hub.Context().Done():
	default:
		hub.internal <- func(h *Hub[META, HEADER]) {
			delete(pack.hotlineExp(), pack.hotline.ID)
		}
	}
}

func (hub *Hub[META, HEADER]) Logger() ILogger {
	return hub.setting.Logger
}

func (hub *Hub[META, HEADER]) Close() {
	hub.setting.CancelFunc()
}

func (hub *Hub[META, HEADER]) clientRegister(cli *Client[META, HEADER]) {

	select {
	case <-hub.Context().Done():
	default:
		hub.internal <- func(h *Hub[META, HEADER]) {
			h.terminalStation.Add(1)

			cli.subMatcher = h.setting.ReceiveMatcher.GenSub()

			// register client
			h.clientBus[cli.ID] = cli
			cli.hotlines = make(map[HotlineID]IHotline)

			go cli.runExecFn()
			// pack the Context , exec init func
			cli.ExecAsync(func() {
				h.setting.OnConnectionStart(cli, cli.meta)
			})
		}
	}

}

func (hub *Hub[META, HEADER]) clientUnregister(cli *Client[META, HEADER]) {

	select {
	case <-hub.Context().Done():
	default:
		hub.internal <- func(h *Hub[META, HEADER]) {
			for _, hotline := range cli.hotlines {
				hotline.Hangup(cli)
			}

			delete(hub.clientBus, cli.ID)
			hub.terminalStation.Done()
		}
	}

}
