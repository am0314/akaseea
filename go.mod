module gitlab.com/am0314/akaseea

go 1.22.6

require (
	github.com/brianvoe/gofakeit v3.18.0+incompatible
	github.com/google/uuid v1.6.0
	github.com/gorilla/websocket v1.5.3
	go.uber.org/zap v1.25.0
)

require github.com/stretchr/testify v1.9.0 // indirect

require go.uber.org/multierr v1.11.0 // indirect
