package akaseea

import (
	"context"
	"sync/atomic"
	"time"
)

// OperationCountdown 該功能主要用於為房間建立一個等待輸入選項
type OperationCountdown[Obj any] struct {
	IHotlineRoomControl[Obj]
	ctx             context.Context
	ctxClose        func()
	isDone          atomic.Bool //是否調用CloseOperation 如果未調用 結束時會走超時流程
	timeoutCallback func(hotline IHotlineRoomControl[Obj], obj *Obj)
}
type IOperationCountdown[Obj any] interface {
	IHotlineRoomControl[Obj]
	Operation(func(hotline IOperationCountdown[Obj], obj *Obj))
	Done()
}

func (ho hotlineRoom[Obj]) OperationTimeout(
	timeOut time.Duration,
	timeoutCallback func(hotline IHotlineRoomControl[Obj], obj *Obj),
) IOperationCountdown[Obj] {
	opCtx, opClose := context.WithTimeout(ho.ctx, timeOut)
	res := &OperationCountdown[Obj]{
		ctx:             opCtx,
		ctxClose:        opClose,
		timeoutCallback: timeoutCallback,
	}
	go res.run()
	return res
}
func (o *OperationCountdown[Obj]) run() {
	<-o.ctx.Done()
	if !o.isDone.Load() {
		o.IHotlineRoomControl.ExecAsync(o.timeoutCallback)
	}
}
func (o *OperationCountdown[Obj]) Operation(operationDo func(hotline IOperationCountdown[Obj], obj *Obj)) {
	o.IHotlineRoomControl.ExecAsync(func(hotline IHotlineRoomControl[Obj], obj *Obj) {
		operationDo(o, obj)
	})
}
func (o *OperationCountdown[Obj]) Done() {
	o.isDone.Store(true)
	o.ctxClose()
}

type OperationCountdownTicker[Obj any] struct {
	IHotlineRoomControl[Obj]
	ctx             context.Context
	ctxClose        func()
	isDone          atomic.Bool //是否調用CloseOperation 如果未調用 結束時會走超時流程
	timeoutCallback func(hotline IHotlineRoomControl[Obj], obj *Obj)
	pollingInterval time.Duration
	tickerCallback  func(hotline IHotlineRoomControl[Obj], obj *Obj)
}
type IOperationCountdownTicker[Obj any] interface {
	IHotlineRoomControl[Obj]
	Operation(func(hotline IOperationCountdownTicker[Obj], obj *Obj))
	Done()
}

func (ho hotlineRoom[Obj]) OperationTicker(
	timeOut time.Duration,
	timeoutCallback func(hotline IHotlineRoomControl[Obj], obj *Obj),
	pollingInterval time.Duration,
	tickerCallback func(hotline IHotlineRoomControl[Obj], obj *Obj),
) IOperationCountdownTicker[Obj] {
	opCtx, opClose := context.WithTimeout(ho.ctx, timeOut)
	res := &OperationCountdownTicker[Obj]{
		ctx:             opCtx,
		ctxClose:        opClose,
		timeoutCallback: timeoutCallback,
		pollingInterval: pollingInterval,
		tickerCallback:  tickerCallback,
	}
	go res.run()
	return res
}
func (o *OperationCountdownTicker[Obj]) run() {
	ticker := time.NewTicker(o.IHotlineRoomControl.LatencyTime(o.pollingInterval))
	for {
		select {
		case <-o.ctx.Done():
			if !o.isDone.Load() {
				o.IHotlineRoomControl.ExecAsync(o.timeoutCallback)
			}
		case <-ticker.C:
			o.IHotlineRoomControl.ExecAsync(o.tickerCallback)
		}
	}

}
func (o *OperationCountdownTicker[Obj]) Operation(operationDo func(hotline IOperationCountdownTicker[Obj], obj *Obj)) {
	o.IHotlineRoomControl.ExecAsync(func(hotline IHotlineRoomControl[Obj], obj *Obj) {
		operationDo(o, obj)
	})
}
func (o *OperationCountdownTicker[Obj]) Done() {
	o.isDone.Store(true)
	o.ctxClose()
}
