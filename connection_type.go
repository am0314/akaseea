package akaseea

type ConnectionType string
type ConnectionProtocal string

const (
	Websocket ConnectionType = "Websocket"
	Socket    ConnectionType = "Socket"
)

const (
	TCP ConnectionProtocal = "tcp"
	UDP ConnectionProtocal = "udp"
)
