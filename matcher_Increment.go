package akaseea

func NewIncrementByte(b byte) *IncrementByte {
	return &IncrementByte{Count: b}
}

// func (m IncrementByte) Less(o IncrementByte) bool {
// 	return m.Count < o.Count
// }

// func (m IncrementByte) Equal(o IncrementByte) bool {
// 	return *m.Count == *o.Count
// }

func (m *IncrementByte) Inc() {
	m.Count++
}

func (m *IncrementByte) Copy() IncrementByte {
	return IncrementByte{Count: m.Count}
}
