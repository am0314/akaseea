package akaseea

import (
	"context"
	"encoding/json"
	"errors"

	"github.com/gorilla/websocket"
)

type HandleFunc[META any] func(IOContext, META)

func (h HandleFunc[META]) Check() HandleFunc[META] {
	if h == nil {
		return func(IOContext, META) {}
	}
	return h
}

// IO as interface omit , diff between Context , is not need [META any]
type IOContext interface {
	Payload() []byte
	// when message flow to hub handle , Client may be Receive new message
	// PayloadHub provide context data , so if use in OnClientReceiveOption()
	// you will get last requested data
	// PayloadHub() []byte
	Logger() ILogger
	// Write is Server send message to Client ,
	// it will send to chan , is safe
	GetContext() context.Context
	Write([]byte) uint64
	WriteJson(any) (uint64, error)
	WriteString(string) uint64
	WriteSerialization(any) (uint64, error)
	WriteCount() uint64
	WriteSplicing(data any, splicing ...byte) (uint64, error)
	WriteMessageType(int, []byte) uint64
	IsWriteConsumed(uint64) bool
	Close()
	HubBroadcast(data []byte)
	Context() context.Context

	ConnectionID() ClientID
	Hub() IHub
	WsConn() *websocket.Conn
	Meta() any
	hotlineExp() map[HotlineID]IHotline

	ExecAsync(func())
	ExecSync(func() error) error
}

func (c *Client[META, HEADER]) Payload() []byte {
	return c.PayloadBuffer
}

func (c *Client[META, HEADER]) Logger() ILogger {
	return c.hub.setting.Logger
}

func (c *Client[META, HEADER]) GetContext() context.Context {
	return c.context
}

func (c *Client[META, HEADER]) Write(data []byte) uint64 {
	return c.writekernel(messagePack{message: data, messageType: c.hub.setting.WebsocketPeriod.WriteMessageType})
}
func (c *Client[META, HEADER]) writekernel(data messagePack) uint64 {
	select {
	case <-c.context.Done():
	default:
		c.write <- data
	}
	return c.writeCount
}

func (c *Client[META, HEADER]) WriteJson(data any) (uint64, error) {
	b, err := json.Marshal(data)

	if err != nil {
		return 0, err
	}

	return c.Write(b), nil
}

func (c *Client[META, HEADER]) WriteString(data string) uint64 {
	return c.Write([]byte(data))
}

func (c *Client[META, HEADER]) WriteSerialization(data any) (uint64, error) {
	b, err := c.hub.setting.ReceiveMatcher.Marshal(data)
	if err != nil {
		return 0, err
	}
	return c.Write(b), nil
}
func (c *Client[META, HEADER]) WriteMessageType(msgType int, message []byte) uint64 {
	return c.writekernel(messagePack{message, messageType(msgType)})
}

func (c *Client[META, HEADER]) WriteCount() uint64 {
	return c.writeCount
}

func (c *Client[META, HEADER]) WriteSplicing(data any, splicing ...byte) (uint64, error) {
	b, err := c.hub.Marshal(data)
	if err != nil {
		return 0, err
	}
	return c.Write(append(splicing, b...)), nil
}
func (c *Client[META, HEADER]) IsWriteConsumed(u uint64) bool {
	return c.writeCount > u
}

func (c *Client[META, HEADER]) Close() {
	c.cancelFunc()
}

func (c *Client[META, HEADER]) Sub() META {
	return c.meta
}

func (c *Client[META, HEADER]) HubBroadcast(data []byte) {
	c.hub.HubBroadcast(data)
}

func (c *Client[META, HEADER]) ConnectionID() ClientID {
	return c.ID
}

func (c *Client[META, HEADER]) Hub() IHub {
	return c.hub
}

func (c *Client[META, HEADER]) WsConn() *websocket.Conn {
	return c.websocketConn
}
func (c *Client[META, HEADER]) Meta() any {
	return c.meta
}
func (c *Client[META, HEADER]) Context() context.Context {
	return c.context
}

func (c *Client[META, HEADER]) ExecAsync(exec func()) {
	select {
	case <-c.context.Done():
	default:
		c.runExec <- exec
	}
}

func (c *Client[META, HEADER]) ExecSync(work func() error) error {
	ack := make(chan error, 1)
	select {
	case <-c.context.Done():
		return errors.New("client is close")
	default:
		c.runExec <- func() {
			ack <- work()
		}
	}
	return <-ack
}
