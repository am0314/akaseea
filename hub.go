package akaseea

import (
	"context"
	"fmt"
	"sync"
	"time"
)

var (
	DefaultNewline = []byte{'\n'}
	DefaultSpace   = []byte{' '}
)
var _ IHub = &Hub[struct{}, struct{}]{}

type Hub[META any, HEADER comparable] struct {
	setting HubSetting[META, HEADER]

	shutDownSign chan struct{}

	internal chan func(*Hub[META, HEADER])

	// id allotment , every connection and Group has id , all use idAllocator to allotment
	idAllocator idAllotment

	// all client register to clientBus
	clientBus map[ClientID]IOContext

	isClose bool

	terminalStation sync.WaitGroup
}

type clientHotlineActionPack struct {
	IOContext
	*hotline
}

type IHub interface {
	DHub
	ExecAsync(func(DHub))
	ExecSync(func(DHub) error) error
}

type DHub interface {
	clientAnswer(clientHotlineActionPack)
	clientHangup(clientHotlineActionPack)
	Logger() ILogger
	Close()
	WaitAdd(int)
	WaitDone()
	Context() context.Context
	Marshal(any) ([]byte, error)
	Unmarshal([]byte, any) error
	HubBroadcast(data []byte)
	allotmentHotlineID() HotlineID
}

func NewHub[META any, HEADER comparable](setting HubSetting[META, HEADER]) *Hub[META, HEADER] {
	settingCheckAndSet(&setting)

	setting.ReceiveMatcher.Init(setting.Logger)

	return &Hub[META, HEADER]{
		setting: setting,
		idAllocator: idAllotment{
			connIDAllotment:    0,
			hotlineIDAllotment: 0,
		},
		shutDownSign: make(chan struct{}, 1),
		clientBus:    make(map[ClientID]IOContext),
		internal:     make(chan func(*Hub[META, HEADER]), setting.ChanQueueSize),
	}
}

func NewHubRun[META any, HEADER comparable](setting HubSetting[META, HEADER]) *Hub[META, HEADER] {
	newHub := NewHub(setting)

	newHub.Run()
	return newHub
}

func (hub *Hub[META, HEADER]) Run() {
	if hub.setting.DelayedExecution != nil {
		hub.setting.DelayedExecution(hub)
	}
	go func() {
		for {
			select {

			case <-hub.setting.ContextOption.Done():
				hub.isClose = true
				time.Sleep(time.Second)
				hub.shutDownSign <- struct{}{}

			case <-hub.shutDownSign:
				if len(hub.clientBus) != 0 {
					hub.setting.Logger.Info(
						fmt.Sprintf("Hub: %s , shutdown retry , wait client num:%d",
							hub.setting.HubName,
							len(hub.clientBus)))
					time.Sleep(time.Second)
					hub.shutDownSign <- struct{}{}
					continue
				}

				hub.setting.Logger.Info(fmt.Sprintf("Hub: %s , in terminal station , wait for next message... ", hub.setting.HubName))
				hub.terminalStation.Wait() //Wait all hotline close
				hub.setting.Logger.Info(fmt.Sprintf("Hub: %s , shutdown success", hub.setting.HubName))
				return
			case internal := <-hub.internal:
				internal(hub)
			}
		}
	}()
}
