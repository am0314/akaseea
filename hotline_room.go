package akaseea

import (
	"context"
	"errors"
	"time"
)

type IHotlineRoomControl[Obj any] interface {
	IHotline
	Pause()
	Resume()
	ExecAsync(fn func(hotline IHotlineRoomControl[Obj], obj *Obj))
	ExecSync(fn func(hotline IHotlineRoomControl[Obj], obj *Obj) error) error
	Close()
	LatencyTime(t time.Duration) time.Duration // 房間若為10Hz 參數為1秒 則回傳1/10秒的time.Duration 通常用於倒計時
	OperationTimeout(
		timeOut time.Duration,
		timeoutCallback func(hotline IHotlineRoomControl[Obj], obj *Obj)) IOperationCountdown[Obj]
	OperationTicker(
		timeOut time.Duration,
		timeoutCallback func(hotline IHotlineRoomControl[Obj], obj *Obj),
		pollingInterval time.Duration,
		tickerCallback func(hotline IHotlineRoomControl[Obj], obj *Obj),
	) IOperationCountdownTicker[Obj]
}

type hotlineRoom[Obj any] struct {
	IHotline
	HotlineRoomOption[Obj]
}
type HotlineRoomOption[Obj any] struct {
	ctx        context.Context
	tick       *time.Ticker
	closeFunc  func()
	ControlSze int
	control    chan func(hotline IHotlineRoomControl[Obj], obj *Obj)
	Hz         int64 // if Hz HzBasic both zero , Logic is skip
	HzBasic    time.Duration
	latency    time.Duration
	NewObj     func() *Obj
	OnStart    func(hotline IHotlineRoomControl[Obj], obj *Obj)
	Logic      func(hotline IHotlineRoomControl[Obj], obj *Obj)
	OnClose    func(hotline IHotlineRoomControl[Obj], obj *Obj)
}

func (ho hotlineRoom[Obj]) LatencyTime(t time.Duration) time.Duration {
	return t / time.Duration(ho.Hz)
}

func (ho hotlineRoom[Obj]) Pause() {
	ho.tick.Reset(time.Hour * 24)
}
func (ho hotlineRoom[Obj]) Resume() {
	ho.tick.Reset(ho.latency)
}

func (ho hotlineRoom[Obj]) ExecAsync(fn func(hotline IHotlineRoomControl[Obj], obj *Obj)) {
	select {
	case <-ho.ctx.Done():
		return
	default:
		ho.control <- fn
	}
}

func (ho hotlineRoom[Obj]) ExecSync(fn func(hotline IHotlineRoomControl[Obj], obj *Obj) error) error {
	ack := make(chan error, 1)
	select {
	case <-ho.ctx.Done():
		return errors.New("client is closed")
	default:
		ho.control <- func(hotline IHotlineRoomControl[Obj], obj *Obj) {
			ack <- fn(hotline, obj)
		}
	}
	return <-ack
}

func (ho hotlineRoom[Obj]) Close() {
	ho.closeFunc()
}

func (ho HotlineRoomOption[Obj]) MakeRoomRun(hotline IHotline) IHotlineRoomControl[Obj] {
	iho, err := ho.MakeRoomRunWithError(hotline)
	if err != nil {
		panic(err.Error())
	}
	return iho
}

func (ho HotlineRoomOption[Obj]) MakeRoomRunWithError(hotline IHotline) (IHotlineRoomControl[Obj], error) {
	if ho.ControlSze == 0 {
		ho.ControlSze = 8
	}

	bothZero := ho.HzBasic == 0 && ho.Hz == 0
	if (ho.HzBasic == 0 && ho.Hz != 0) || (ho.HzBasic != 0 && ho.Hz == 0) {
		return nil, errors.New("HzBasic and Hz must both be set or both be zero")
	}
	if !bothZero {
		ho.latency = ho.HzBasic / time.Duration(ho.Hz)
	}

	ho.control = make(chan func(hotline IHotlineRoomControl[Obj], obj *Obj), ho.ControlSze)
	ho.ctx, ho.closeFunc = context.WithCancel(hotline.context())
	var iho IHotlineRoomControl[Obj] = hotlineRoom[Obj]{
		IHotline:          hotline,
		HotlineRoomOption: ho,
	}

	if bothZero {
		go func() {
			objEntity := ho.NewObj()
			ho.OnStart(iho, objEntity)
			defer ho.tick.Stop()
			for {
				select {
				case fn := <-ho.control:
					if fn != nil {
						fn(iho, objEntity)
					}
				case <-ho.ctx.Done():
					ho.OnClose(iho, objEntity)
					return
				}
			}
		}()
	} else {
		go func() {
			objEntity := ho.NewObj()
			ho.OnStart(iho, objEntity)
			ho.tick = time.NewTicker(ho.latency)
			defer ho.tick.Stop()
			for {
				select {
				case fn := <-ho.control:
					if fn != nil {
						fn(iho, objEntity)
					}
				case <-ho.ctx.Done():
					ho.OnClose(iho, objEntity)
					return
				case <-ho.tick.C:
					ho.Logic(iho, objEntity)
				}
			}
		}()
	}

	return iho, nil
}
