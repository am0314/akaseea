package akaseea

import (
	"context"

	"github.com/google/uuid"
)

type HubSetting[META any, HEADER comparable] struct {
	// if empty , set uuid
	HubName string

	// if nil , will gen to background
	ContextOption context.Context
	CancelFunc    context.CancelFunc

	// Logger
	//  It can be empty, it is only used for the internal output of the Hub, and it is preset as a logger that is simply wrapped with log

	Logger ILogger

	// when create new client , need to gen a sub into client
	// must be set , you can use fn()struct{}{return struct{}{}}
	// if nil , will panic
	MetaGen func() META

	ReceiveMatcher IMasterMatcher[META, HEADER]

	// when client connection message to server , how to handle the data
	// in this stage , the payload is nil , do not use
	// if nil ,
	//  default func(IOContext, META) {}
	OnConnectionStart HandleFunc[META]

	// OnConnectionClose when connection close
	//  default func(IOContext, META) {}
	OnConnectionClose HandleFunc[META]

	// when client send message to server , after OnClientReceive , OnHubReceiveOption will handle in hub goroutine
	// so if using time.sleep in OnHubReceiveOption , entire hub will be stark
	// if nil , default return
	// OnHubReceiveOption HandleFunc[META,HEADER]

	// what have to do before connection close
	// in this stage , the payload is nil , do not use
	// if nil , default return
	// OnHubConnectionClose HandleFunc[META,HEADER]

	// every this struct mark chan size depends on this value
	// determines the maximum allowable buffer for handling spikes
	//  default 8
	ChanQueueSize uint

	// WebsocketPeriod setting websocket Period
	// default &WebsocketPeriod{
	// 	PingPeriod: ((60 * time.Second) * 9) / 10,
	// 	PongWait:   60 * time.Second,
	// 	WriteWait:  10 * time.Second,
	// 	WsPongHandler: func(c *websocket.Conn) func(string) error {
	// 		return func(string) error {
	// 			c.SetReadDeadline(time.Now().Add(60 * time.Second))
	// 			return nil
	// 		}
	// 	},
	// 	WsWriteTimeOutHandle: func(c *websocket.Conn) {
	// 		c.SetWriteDeadline(time.Now().Add(10 * time.Second))
	// 		if err := c.WriteMessage(websocket.PingMessage, nil); err != nil {
	// 			set.Logger.Warn(err.Error())
	// 		}
	// 		return
	// 	},
	// }
	WebsocketPeriod *WebsocketPeriod[META]

	// SocketPeriod setting socket Period
	SocketPeriod *SocketPeriod

	// This content will be executed before the hub is officially launched.
	DelayedExecution func(IHub)
}

func settingCheckAndSet[META any, HEADER comparable](set *HubSetting[META, HEADER]) {
	if set.HubName == "" {
		set.HubName = uuid.New().String()
	}
	if set.ContextOption == nil {
		set.ContextOption, set.CancelFunc = context.WithCancel(context.Background())
	}

	if set.WebsocketPeriod != nil {
		if set.WebsocketPeriod.WriteMessageType == 0 {
			set.WebsocketPeriod.WriteMessageType = TextMessage
		}
	}

	// LoggerOption check
	if set.Logger == nil {
		set.Logger = DefaultLogger()
	}

	// SubGen check
	if set.MetaGen == nil {
		set.Logger.Panic("set.SubGen can't be empty")
	}

	set.OnConnectionStart = set.OnConnectionStart.Check()
	set.OnConnectionClose = set.OnConnectionClose.Check()

	if set.ChanQueueSize == 0 {
		set.ChanQueueSize = 8
	}

	if set.WebsocketPeriod == nil {
		set.WebsocketPeriod = DefaultWebsocketPeriod[META]()
	} else {
		if set.WebsocketPeriod.ReadBufferPreprocessing == nil {
			set.Logger.Panic("set.WebsocketPeriod.ReadBufferPreprocessing can't be empty")
		}
	}

	if set.SocketPeriod == nil {
		// TODO
	}

}
