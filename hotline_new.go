package akaseea

import "context"

func (hub *Hub[META, HEADER]) NewHotline() IHotline {
	option := HotlineOption{
		Logger: hub.Logger(),
	}
	return hub.NewHotlineWithOption(option)
}
func (hub *Hub[META, HEADER]) NewHotlineWithOption(option HotlineOption) IHotline {
	ctx, cancel := context.WithCancel(hub.Context())
	if option.Logger == nil {
		option.Logger = hub.Logger()
	}
	newHotline := &hotline{
		ID:                hub.allotmentHotlineID(),
		internal:          make(chan func(*hotline), option.QueueSize),
		chatMember:        map[ClientID]IOContext{},
		hub:               hub,
		HotlineBuffer:     []byte{},
		ctx:               ctx,
		contextCancelFunc: cancel,
		option:            option,
	}

	// Chatpods run
	go newHotline.hotlineRun()

	return newHotline
}
