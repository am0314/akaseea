package akaseea

import (
	"reflect"
)

func (matcher *MatcherAuto[META, HEADER]) Init(logger ILogger) {

	matcher.handleMap = make(map[HEADER]HandlePack)
	matcher.routeMap = make(map[HEADER]string)

	vofHandleSetting := reflect.ValueOf(matcher.HandleSetting)

	header := matcher.ZeroHeader

	for i := 0; i < vofHandleSetting.NumMethod(); i++ {
		handle := vofHandleSetting.Method(i).Interface()
		copyHeader := header.Copy()

		if handle == nil {
			logger.Warn("Handle is nil, header: ", header)
			continue
		}

		handleFuncName, handleFunc, handleRequest, err := handleCheck(handle, header)
		if err != nil {
			logger.Error(err.Error())
			continue
		}

		matcher.routeMap[copyHeader] = handleFuncName

		nextHandlePack := HandlePack{
			Handle:     handleFunc,
			ParsingBox: handleRequest,
		}

		matcher.handleMap[copyHeader] = nextHandlePack
		header.Inc()
	}
}

func (matcher *MatcherAuto[META, HEADER]) GenSub() SubMatcher[META, HEADER] {
	handleCopy := make(map[HEADER]HandlePack)
	for k, v := range matcher.handleMap {

		tof := reflect.ValueOf(v.ParsingBox).Elem().Type()

		handleCopy[k] = HandlePack{
			Handle:            v.Handle,
			ParsingBox:        reflect.New(tof).Interface(),
			SkipSerialization: v.SkipSerialization,
		}
	}

	return SubMatcher[META, HEADER]{
		master:    matcher,
		handleMap: handleCopy,
	}
}

func (matcher *MatcherAuto[META, HEADER]) HeaderParsing(ReceiveData []byte) (HEADER, []byte) {
	return matcher.HeaderParsingFunc(ReceiveData)
}

func (matcher *MatcherAuto[META, HEADER]) Unmarshal(body []byte, handleRequest any) error {
	return matcher.UnmarshalFunc(body, handleRequest)
}

func (matcher *MatcherAuto[META, HEADER]) Marshal(handleRequest any) ([]byte, error) {
	return matcher.MarshalFunc(handleRequest)
}
