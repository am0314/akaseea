package akaseea

import (
	"context"
	"time"
)

type LongPollingOption struct {
	Retry           int                          // if zero , do for timeout
	Timeout         time.Duration                // must
	Frequency       time.Duration                // if zero , set 1 sec
	Do              func(ihub IHub, done func()) // If shared data operations are involved, hub.exec should be called
	TimeoutCallback func(ihub IHub)              // Notes are the same as Do
}
type LongPollingResult struct {
}

func (hub *Hub[META, HEADER]) LongPolling(option LongPollingOption) error {
	if option.Timeout <= 0 {
		return ErrLongPollingTimeOutZeroError
	}
	if option.Frequency == 0 {
		option.Frequency = time.Second
	}
	lpCtx, lpClose := context.WithTimeout(hub.Context(), option.Timeout)

	defer lpClose()
	go func() {
		isDone := false
		closePack := func() {
			isDone = true
			lpClose()
		}

		ticker := time.NewTicker(option.Frequency)
		for i := 0; option.Retry == 0 || (option.Retry >= 0 && i < option.Retry); i++ {
			select {
			case <-ticker.C:
				option.Do(hub, closePack)

			case <-lpCtx.Done():
				if !isDone {
					option.TimeoutCallback(hub)
				}
				return
			}
		}
	}()
	return nil
}
