package akaseea

import (
	"reflect"
)

// If you want to skip the serialization method and return the value, use this
type Empty struct{}

var empty *Empty = &Empty{}

type IMasterMatcher[META any, HEADER comparable] interface {
	Init(ILogger)
	GenSub() SubMatcher[META, HEADER]
	HeaderParsing([]byte) (HEADER, []byte)
	Unmarshal([]byte, any) error
	Marshal(any) ([]byte, error)
}

type SubMatcher[META any, HEADER comparable] struct {
	master    IMasterMatcher[META, HEADER]
	handleMap map[HEADER]HandlePack
}

type HandlePack struct {
	Handle            reflect.Value
	ParsingBox        any
	SkipSerialization bool
}

type MatcherMap[META any, HEADER comparable] struct {
	// HeaderParsingFunc will be use to parsing header and payload from Receive data
	HeaderParsingFunc func([]byte) (header HEADER, payload []byte)

	// HandleSetting only check type is "" will be accept
	//
	// otherwise will be panic
	//
	// example:
	// 	func(IOContext, META, any)
	HandleSetting map[HEADER]any

	MarshalFunc   func(any) ([]byte, error)
	UnmarshalFunc func([]byte, any) error

	routeMap  map[HEADER]string
	handleMap map[HEADER]HandlePack
}

type IncrementByte struct {
	Count byte
}

type IncrementHeader[HEADERMETA comparable] interface {
	Inc()
	Copy() HEADERMETA
}

type MatcherAuto[META any, HEADER comparable] struct {
	// HeaderParsingFunc will be use to parsing header and payload from Receive data
	HeaderParsingFunc func([]byte) (header HEADER, payload []byte)

	ZeroHeader IncrementHeader[HEADER]

	// HandleSetting only check type is "" will be accept
	//
	// otherwise will be panic
	//
	// example:
	// 	func(IOContext, META, any)
	HandleSetting any

	MarshalFunc   func(any) ([]byte, error)
	UnmarshalFunc func([]byte, any) error

	routeMap  map[HEADER]string
	handleMap map[HEADER]HandlePack
}
