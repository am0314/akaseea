package akaseea

import (
	"fmt"
	"reflect"
)

func (matcher *SubMatcher[META, HEADER]) Receive(cli *Client[META, HEADER], ReceiveData []byte) error {
	var header HEADER
	header, cli.PayloadBuffer = matcher.master.HeaderParsing(ReceiveData)
	handle, handleExist := matcher.handleMap[header]
	if !handleExist {
		return fmt.Errorf(ErrHandleNotFind, header)
	}

	if !handle.SkipSerialization {
		if err := matcher.master.Unmarshal(cli.PayloadBuffer, handle.ParsingBox); err != nil {
			return err
		}
	}

	handle.Handle.Call([]reflect.Value{reflect.ValueOf(cli), reflect.ValueOf(cli.meta), reflect.ValueOf(handle.ParsingBox)})
	return nil
}
