package akaseea

import (
	"context"
)

type hotline struct {
	ID            HotlineID
	internal      chan func(*hotline)
	chatMember    map[ClientID]IOContext
	hub           IHub
	HotlineBuffer []byte
	// answerChan        chan IOContext
	// hangupChan        chan IOContext
	// pushChan          chan []byte
	ctx               context.Context
	contextCancelFunc context.CancelFunc
	isClose           bool
	pushCount         uint64
	option            HotlineOption
}
type HotlineOption struct {
	Logger    ILogger
	QueueSize int
}

type IHotline interface {
	// Answer will send any IHotline broadcast to IOContext
	Answer(IOContext)
	Hangup(IOContext)
	// Push does not copy data, so sharing security is not guaranteed.
	//
	//  The current pushCount will be returned when the call is made.
	//
	//  You can then call pushCount to hotline to check whether the previous message has been consumed.
	Push(data []byte) uint64
	PushCount() uint64
	IsPushConsumed(uint64) bool
	MembersNum() int
	Logger() ILogger

	Close()
	CloseIfEmpty() (isCloseAfterCall bool)
	IsClosed() bool
	context() context.Context
}
