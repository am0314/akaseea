package akaseea

import (
	"context"
	"net"

	"github.com/gorilla/websocket"
)

type Client[META any, HEADER comparable] struct {
	ID            ClientID
	context       context.Context
	cancelFunc    context.CancelFunc
	hub           *Hub[META, HEADER]
	websocketConn *websocket.Conn
	socketConn    net.Conn
	write         chan messagePack
	// ReceiveBuffer only Receive , after processing
	ReceiveBuffer []byte
	PayloadBuffer []byte

	subMatcher SubMatcher[META, HEADER]

	hotlines map[HotlineID]IHotline

	// meta represents user-specific data that the client carries. This can include metadata, attributes, or any other additional information specific to the client.
	meta       META
	writeCount uint64

	runExec    chan func()
	finishSign chan struct{}
}
type messagePack struct {
	message     []byte
	messageType messageType
}

func (c *Client[MATE, HEADER]) hotlineExp() map[HotlineID]IHotline {
	return c.hotlines
}

func (c *Client[MATE, HEADER]) runExecFn() {
	go c.closeWait()
	for {
		select {
		case <-c.finishSign:
			close(c.runExec)
			for exec := range c.runExec {
				exec()
			}
			c.hub.setting.OnConnectionClose(c, c.meta)
			if c.websocketConn != nil {
				c.websocketConn.Close()
			}

			c.hub.clientUnregister(c)
			return
		case exec, o := <-c.runExec:
			if o {
				exec()
			}
		}
	}
}
func (c *Client[MATE, HEADER]) closeWait() {
	<-c.context.Done()
	close(c.finishSign)
}
