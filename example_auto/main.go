package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/brianvoe/gofakeit"
	"gitlab.com/am0314/akaseea"
	"go.uber.org/zap"
)

// exampleServer struct
type exampleServer struct {
	MainHub *akaseea.Hub[*ExampleClient, akaseea.IncrementByte]

	Logger *zap.Logger

	// chat管理器
	chatManager *chatManager
}
type chatManager struct {
	chatChanIDAllocator int
	chatPods            map[int]chatChan
}

type chatChan struct {
	Title   string           `json:"title"`
	Hotline akaseea.IHotline `json:"-"`
}

// Client carry sub struct
type ExampleClient struct {
	ConnName        string
	currentChatRoom *chatChan
}

func main() {
	z, _ := zap.NewDevelopment()
	theServer := &exampleServer{
		Logger: z,
	}

	// init the hub setting
	setting := akaseea.HubSetting[*ExampleClient, akaseea.IncrementByte]{
		Logger: nil,
		MetaGen: func() *ExampleClient {
			return &ExampleClient{
				ConnName: gofakeit.Name(),
			}
		},
		ReceiveMatcher: &akaseea.MatcherAuto[*ExampleClient, akaseea.IncrementByte]{
			HeaderParsingFunc: func(data []byte) (akaseea.IncrementByte, []byte) {
				return akaseea.IncrementByte{Count: data[0]}, data[1:]
			},
			ZeroHeader:    akaseea.NewIncrementByte('0'),
			HandleSetting: theServer,
			MarshalFunc:   json.Marshal,
			UnmarshalFunc: json.Unmarshal,
		},

		OnConnectionStart: func(i akaseea.IOContext, ec *ExampleClient) {
			i.Write([]byte("this is start message and this your name" + ec.ConnName))
		},
		OnConnectionClose: func(i akaseea.IOContext, ec *ExampleClient) {
			fmt.Printf("user [%s] leave us \n", ec.ConnName)
			time.Sleep(time.Second)
		},
	}

	theServer.MainHub = akaseea.NewHubRun(setting)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "home.html")
	})

	http.HandleFunc("/ws", theServer.MainHub.WebSocketHandler)
	http.ListenAndServe("0.0.0.0:80", nil)
}

type RequestCreateChatChan struct {
	Name string `json:"name"`
}

func (es *exampleServer) CreateChatChan(ctx akaseea.IOContext, ec *ExampleClient, request *RequestCreateChatChan) {
	fmt.Println("create chat chan", request)
	ctx.Write([]byte("create chat chan"))
}
