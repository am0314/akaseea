package akaseea

import (
	"fmt"
	"reflect"
	"runtime"
)

func handleCheck(handle, header any) (
	handleFuncName string, handleFunc reflect.Value, handreq any, err error) {

	handleTof := reflect.TypeOf(handle)

	if handleTof.Kind() != reflect.Func {
		return "", reflect.Value{}, nil, fmt.Errorf(ErrHandleKindError, header)
	}

	if handleTof.NumIn() != 3 {
		return "", reflect.Value{}, nil, fmt.Errorf(ErrHandleKindError, header)
	}
	if handleTof.In(2).Kind() != reflect.Ptr {
		return "", reflect.Value{}, nil, fmt.Errorf(ErrHandleKindError, header)
	}
	if handleTof.NumOut() != 0 {
		return "", reflect.Value{}, nil, fmt.Errorf(ErrHandleKindError, header)
	}

	if handleTof.In(0).Kind() != reflect.Interface {
		return "", reflect.Value{}, nil, fmt.Errorf(ErrHandleKindError, header)
	}
	if handleTof.In(0).Name() != "IOContext" {
		return "", reflect.Value{}, nil, fmt.Errorf(ErrHandleKindError, header)
	}

	typeOfHandleRequest := handleTof.In(2)
	handleRequest := reflect.New(typeOfHandleRequest.Elem())
	handleRequest.Elem().Set(reflect.Zero(typeOfHandleRequest.Elem()))

	return runtime.FuncForPC(reflect.ValueOf(handle).Pointer()).Name(),
		reflect.ValueOf(handle),
		handleRequest.Interface(), nil
}
