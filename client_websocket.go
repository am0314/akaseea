package akaseea

import (
	"context"
	"net/http"
	"sync/atomic"

	"github.com/gorilla/websocket"
)

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.

// WebSocketHandler handles websocket requests from the peer.
func (hub *Hub[META, HEADER]) WebSocketHandler(w http.ResponseWriter, r *http.Request) {
	hub.WebSocketHandlerWithMeta(w, r, hub.setting.MetaGen())
}

// like BuildingWebSocketHandles , but can assign meta direct
//
// using this will skip hub.setting.MetaGen()
func (hub *Hub[META, HEADER]) WebSocketHandlerWithMeta(w http.ResponseWriter, r *http.Request, m META) {
	if hub.isClose {
		return
	}
	conn, err := hub.setting.WebsocketPeriod.Upgrader.Upgrade(w, r, nil)
	if err != nil {
		hub.Logger().Error("Upgrader fail:", err)
		return
	}

	context, cancelFunc := context.WithCancel(hub.setting.ContextOption)

	client := &Client[META, HEADER]{
		ID:            hub.idAllocator.allotmentConnectionID(),
		context:       context,
		cancelFunc:    cancelFunc,
		hub:           hub,
		websocketConn: conn,
		socketConn:    nil,
		write:         make(chan messagePack, hub.setting.ChanQueueSize),
		ReceiveBuffer: nil,
		meta:          m,
		runExec:       make(chan func(), hub.setting.ChanQueueSize),
		finishSign:    make(chan struct{}, 1),
	}

	if hub.setting.WebsocketPeriod.PingPongPlan.PongHandler != nil {
		conn.SetPongHandler(
			func(appData string) error {
				return hub.setting.WebsocketPeriod.PingPongPlan.PongHandler(client, m, appData)
			})
	}

	if hub.setting.WebsocketPeriod.PingPongPlan.PingHandler != nil {
		conn.SetPingHandler(
			func(appData string) error {
				return hub.setting.WebsocketPeriod.PingPongPlan.PingHandler(client, m, appData)
			})
	}

	go client.websocketWritePump()
	go client.websocketReadPump()

	client.hub.clientRegister(client)
}

func (c *Client[META, HEADER]) websocketReadPump() {
	for {
		var err error
		_, c.ReceiveBuffer, err = c.websocketConn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, c.hub.setting.WebsocketPeriod.ExpectedWsError...) {
				c.hub.Logger().Error("websocketReadPump error: %s", err.Error())
			}
			c.cancelFunc()
			return
		}

		c.ReceiveBuffer = c.hub.setting.WebsocketPeriod.ReadBufferPreprocessing(c.ReceiveBuffer)

		sceBuff := make([]byte, len(c.ReceiveBuffer))
		copy(sceBuff, c.ReceiveBuffer)
		c.ExecAsync(func() {
			err = c.subMatcher.Receive(c, sceBuff)
			if err != nil {
				c.Logger().Warn(err.Error())
			}
		})
	}
}

func (c *Client[META, HEADER]) websocketWritePump() {
	defer c.websocketConn.Close()
	for {
		message, ok := <-c.write
		if !ok {
			// The hub closed the channel.
			c.cancelFunc()
			return
		}

		w, err := c.websocketConn.NextWriter(int(message.messageType))
		if err != nil {
			if err != websocket.ErrCloseSent {
				c.Logger().Warn(err.Error())
				return
			}
			continue
		}

		if _, err = w.Write(append(message.message, c.hub.setting.WebsocketPeriod.WriteNewline...)); err != nil {
			c.Logger().Warn(err.Error())
			continue
		}

		atomic.AddUint64(&c.writeCount, 1)

		if err := w.Close(); err != nil {
			c.Logger().Warn(err.Error())
			continue
		}
	}
}
