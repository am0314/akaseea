package akaseea

import "log"

type ILogger interface {
	Debug(format string, a ...any)
	Info(format string, a ...any)
	Warn(format string, a ...any)
	Error(format string, a ...any)
	Panic(format string, a ...any)
	Fatal(format string, a ...any)
}

type defaultLogger struct {
}

// Fatal implements ILogger
func (defaultLogger) Fatal(format string, a ...any) {
	log.Fatalf("[FATAL]"+format, a...)
}

// Panic implements ILogger
func (defaultLogger) Panic(format string, a ...any) {
	log.Panicf("[PANIC]"+format, a...)
}

// Error implements ILogger
func (defaultLogger) Error(format string, a ...any) {
	log.Printf("[ERROR]"+format, a...)
}

// Info implements ILogger
func (defaultLogger) Info(format string, a ...any) {
	log.Printf("[INFO]"+format, a...)
}

// Warn implements ILogger
func (defaultLogger) Warn(format string, a ...any) {
	log.Printf("[WARN]"+format, a...)
}

func DefaultLogger() ILogger {
	return defaultLogger{}
}
func (defaultLogger) Debug(format string, a ...any) {
	log.Printf("[DEBUG]"+format, a...)
}
