package akaseea

import (
	"context"
	"sync/atomic"
)

// Answer client id
func (c *hotline) Answer(client IOContext) {
	select {
	case <-c.ctx.Done():
		return
	default:
		c.internal <- func(h *hotline) {
			c.hub.clientAnswer(clientHotlineActionPack{
				client,
				c,
			})
			c.chatMember[client.ConnectionID()] = client
		}
	}
}

// Hangup 從該群組取消訂閱
func (c *hotline) Hangup(client IOContext) {
	select {
	case <-c.ctx.Done():
		return
	default:
		c.internal <- func(h *hotline) {
			c.hub.clientHangup(clientHotlineActionPack{
				client,
				c,
			})
			delete(c.chatMember, client.ConnectionID())
		}
	}

}

// Push 針對該群組廣播
func (c *hotline) Push(data []byte) uint64 {
	select {
	case <-c.ctx.Done():
		return c.pushCount
	default:
		c.internal <- func(h *hotline) {
			for _, client := range c.chatMember {
				client.Write(data)
			}
			atomic.AddUint64(&c.pushCount, 1)
		}
		return c.pushCount
	}
}

// Close
func (c *hotline) Close() {
	c.contextCancelFunc()
}

func (c *hotline) PushCount() uint64 {
	return c.pushCount
}

func (c *hotline) IsPushConsumed(u uint64) bool {
	return c.pushCount > u
}

func (c *hotline) CloseIfEmpty() bool {
	select {
	case <-c.ctx.Done():
		return false
	default:
		res := make(chan bool, 1)
		c.internal <- func(h *hotline) {
			if len(h.chatMember) > 0 {
				res <- false
				return
			}
			res <- true
			c.Close()
		}
		return <-res
	}
}

func (c *hotline) IsClosed() bool {
	return c.isClose
}

func (c *hotline) MembersNum() int {
	i := make(chan int)
	c.internal <- func(h *hotline) {
		i <- len(h.chatMember)
	}
	return <-i
}

func (c *hotline) Logger() ILogger {
	return c.option.Logger
}
func (c *hotline) context() context.Context {
	return c.ctx
}
