package akaseea

type DataSize uint64

const (
	_           = iota
	KB DataSize = 1 << (10 * iota)
	MB
	GB
	TB
	PB
	EB
)
