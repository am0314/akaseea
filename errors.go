package akaseea

import "errors"

var (
	ErrHandleAnyNotPtr  = "handle any not ptr: %+v"
	ErrHandleNotFunc    = "handle not func: %+v"
	ErrContextNotClient = "context not client: %+v"

	ErrHandleNotFind   = "handle not find header: %+v"
	ErrHandleKindError = "handle kind error: header %+v"

	ErrLongPollingTimeOutZeroError = errors.New("LongPolling option.Timeout can't be zero")
)
