package akaseea

func (hotline *hotline) hotlineRun() {
	hotline.hub.WaitAdd(1)

	defer func() {
		hotline.hub.WaitDone()
	}()
	for !hotline.isClose {
		select {
		case fn := <-hotline.internal:
			fn(hotline)

		case <-hotline.ctx.Done():
			hotline.isClose = true
		}
	}
	for _, client := range hotline.chatMember {
		hotline.hub.clientHangup(clientHotlineActionPack{
			client,
			hotline,
		})
		delete(hotline.chatMember, client.ConnectionID())
	}
}
